/* eslint-disable func-names */
import 'bootstrap';
import React from 'react';
import ReactDOM from 'react-dom';
import AppRoot from './AppRoot';

function debounce(func, wait, immediate) {
  let timeout;
  return function (...args) {
    const context = this;
    const later = function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };
    const callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
}

class Client {
  constructor() {
    const appEl = document.getElementById('app');
    if (appEl) {
      ReactDOM.render(<AppRoot variant={appEl.dataset.variant} env={appEl.dataset.env}/>, appEl);
    }

    // $(window).resize(debounce(this.onResize.bind(this), 200));
  }
}

const app = new Client();
