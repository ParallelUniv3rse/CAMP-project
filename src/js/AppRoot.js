import React from 'react';
import PropTypes from 'prop-types';
import {TimelineLite, TweenMax} from 'gsap';
import {Power2, Expo} from 'gsap/EasePack';
import ReactSVG from 'react-svg';
import './vendor/DSP';
import {pulse, svgDrawAnimation, fadeBetween} from './helpers/animation';
import AppContext from './contexts/AppContext';
import LanguageContext from './contexts/LanguageContext';
import FormContext, {FormProvider} from './contexts/FormContext';
import Button from './components/Button';
import SliderForm from './components/SliderForm';
import CZ from '../../app/models/locales/cs_CZ';
import EN from '../../app/models/locales/en_US';
import LanguageSelector from "./components/LanguageSelector";
import TimeoutComponent from './components/TimeoutComponent';

const PULSE_INTERVAL = 5000;

class AppRoot extends React.Component {
  static propTypes = {
    variant: PropTypes.string,
    env: PropTypes.string.isRequired,
  };

  state = {
    language: 'CZ',
    copy: CZ,
    startTimestamp: null,
    timeoutActive: false,
  }

  updateLanguage = (langCode) => {
    let state = false;
    switch (langCode) {
      case 'EN':
        state = {
          language: 'EN',
          copy: EN,
        };
        break;
      case 'CZ':
        state = {
          language: 'CZ',
          copy: CZ,
        };
        break;
    }
    if (state) {
      this.setState(state);
    }
  }

  resetPage = (e) => {
    if (e && e.target && !e.defaultPrevented) e.preventDefault();
    window.location.reload();
  }

  componentDidMount() {
    const startButton = this.startButtonRef.root;
    this.pulseInterval = setInterval(() => {
      if (!startButton.matches(':hover') && !startButton.matches(':active')) {
        pulse(startButton);
      }
    }, PULSE_INTERVAL);
  }

  playBackground = (err, svg) => {
    if (err) return;
    const els = [
      svg.querySelectorAll('#riverways path'),
      svg.querySelectorAll('#railways path'),
      svg.querySelectorAll('#major_x5F_roads_1_ path'),
    ];
    this.backgroundTimeline = svgDrawAnimation(svg, els);
  }

  handleStartClick = () => {
    const overlay = document.getElementById('intro-overlay');
    const content = document.getElementById('content');
    this.setState({
      startTimestamp: new Date(),
    });

    clearInterval(this.pulseInterval);
    this.sliderFormRef.setActiveSlide(this.sliderFormRef.swiper.realIndex)
      .then(() =>
        new Promise((resolve, reject) =>
          fadeBetween(overlay, content.querySelector('.section__content'), resolve)
        )
      )
      .then(() => {
        if (this.backgroundTimeline) {
          this.backgroundTimeline.kill();
        }
      })
      .then(() => {
        if (this.props.env !== 'development') {
          this.setState({
            timeoutActive: true,
          });
        }
      });
  }

  render() {
    return (
      <LanguageContext.Provider value={{
        language: this.state.language,
        copy: this.state.copy,
        updateLanguage: this.updateLanguage,
      }}>
        <AppContext.Provider value={{
          ...this.props,
          resetPage: this.resetPage,
        }}>
          <FormProvider variant={this.props.variant}>
            <section className="section section--fullscreen section--overlay section--inverted" id="intro-overlay">
              <ReactSVG src="/static/img/prague.svg" className="section__background" onInjected={this.playBackground}/>
              <div className="text-center" style={{ maxWidth: '65vw' }}>
                <h1>{this.state.copy.intro.title}</h1>
                <h2
                  className="mt-3"
                >{this.state.copy.intro["subtitle_no-reward"]}</h2>
                <Button
                  type="primary-outline"
                  style={{ marginTop: '10vh' }}
                  className="btn--hover-grow"
                  ref={ref => this.startButtonRef = ref}
                  onClick={this.handleStartClick}
                  icon="ion-ios-arrow-round-forward"
                >
                  {this.state.copy.begin}
                </Button>
              </div>
              <LanguageSelector updateLanguage={this.updateLanguage} language={this.state.language}/>
            </section>
            <section className="section section--fullscreen section--inverted" id="content">
              <FormContext.Consumer>
                {({ submitForm }) => (
                  <SliderForm ref={ref => this.sliderFormRef = ref} submitForm={submitForm} startTimestamp={this.state.startTimestamp}/>
                )}
              </FormContext.Consumer>
            </section>
            {this.state.timeoutActive &&
            <TimeoutComponent resetPage={this.resetPage}/>
            }
          </FormProvider>
        </AppContext.Provider>
      </LanguageContext.Provider>
    );
  }
}

export default AppRoot;
