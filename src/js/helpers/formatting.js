export const padNumber = (num, length = 2) => (`0000${num}`).slice(-length);
