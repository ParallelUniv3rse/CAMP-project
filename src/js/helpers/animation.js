import TimelineLite from 'gsap/TimelineLite';
import tinycolor from 'tinycolor2';
import TweenMax, {Power2} from "gsap/TweenMax";

export function pulse(el, color = 'rgb(0, 255, 0)') {
  const tl = new TimelineLite();
  color = tinycolor(color);
  const alpha03 = color.setAlpha(0.3).toRgbString();
  const alpha0 = color.setAlpha(0).toRgbString();
  tl.set(el, { transition: 'none' });
  tl.fromTo(el, 0.7, {
    scale: 1,
    boxShadow: `0 0 0 0 ${alpha03}`,
  }, {
    scale: 1.1,
    boxShadow: `0 0 0 25px ${alpha0}`,
  });
  tl.to(el, 0.3, {
    scale: 1,
    boxShadow: `0 0 0 0 ${alpha0}`,
    clearProps: 'boxShadow, scale',
  });
  tl.set(el, {
    delay: 0.1,
    clearProps: 'transition',
  });
}

export function fadeBetween(el1, el2, onComplete = () => null) {
  const tl = new TimelineLite({ paused: true });
  tl.to(el1, 0.15, {
    opacity: 0,
  }).set(el1, {
    display: 'none',
  }).from(el2, 0.15, {
    opacity: 0,
  });
  TweenMax.to(tl, tl.duration(), { progress: 1, ease: Power2.easeInOut, onComplete });
}

export function buttonClickAnimation(el) {
  const tl = new TimelineLite({ paused: true });
  tl
    .set(el, { transition: 'none' })
    .fromTo(el, 0.2, { scale: 1 }, { scale: 0.96, ease: Power2.easeInOut })
    .to(el, 0.1, { scale: 0.98, ease: Power2.easeInOut, clearProps: 'transform, transition' });
  return tl;
}

export function svgDrawAnimation(svg, els) {
  const tl = new TimelineLite({
    onComplete() {
      this.restart();
    },
  });
  tl.fromTo(svg, 5, { opacity: 0 }, { opacity: 1 });
  tl.staggerFromTo(els[0], 2, {
    drawSVG: '0%',
  }, {
    drawSVG: '100%',
    // ease: Expo.easeIn,
    force3D: true,
  }, 0.5);
  tl.staggerFromTo(els[1], 2, {
    drawSVG: '0%',
  }, {
    drawSVG: '100%',
    // ease: Expo.easeIn,
    force3D: true,
  }, 0.1);
  tl.staggerFromTo(els[2], 2, {
    drawSVG: '0%',
  }, {
    drawSVG: '100%',
    // ease: Expo.easeIn,
    force3D: true,
  }, 0.3);
  tl.to(svg, 3, { opacity: 0, delay: 5 });
  tl.timeScale(1.5);
  return tl;
}
