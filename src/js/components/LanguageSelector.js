import React, {Component} from 'react';
import PropTypes from 'prop-types';

function LanguageSelector(props) {
  return (
    <div className='languageSelector square'>
      <a
        href="#"
        className={props.language === 'CZ' ? 'text-primary' : ''}
        onClick={(e) => {
          e.preventDefault();
          props.updateLanguage('CZ');
        }}
      >CZ</a>
      /
      <a
        href="#"
        className={props.language === 'EN' ? 'text-primary' : ''}
        onClick={(e) => {
          e.preventDefault();
          props.updateLanguage('EN');
        }}
      >EN</a>
    </div>
  );
};

LanguageSelector.propTypes = {
  updateLanguage: PropTypes.func.isRequired,
  language: PropTypes.string.isRequired,
};

export default LanguageSelector;
