/* eslint-disable react/prop-types,arrow-parens */
import React from 'react';
import PropTypes from 'prop-types';
import SelectQuestion from './questions/SelectQuestion';
import MultiselectQuestion from './questions/MultiselectQuestion';
import TextQuestion from './questions/TextQuestion';
import RangeQuestion from './questions/RangeQuestion';
import DropdownQuestion from './questions/DropdownQuestion';
import FormContext from '../contexts/FormContext';
import LanguageContext from '../contexts/LanguageContext';

class Question extends React.Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    question: PropTypes.object.isRequired,
    handleNextClick: PropTypes.func.isRequired,
  };

  render() {
    return (
      <LanguageContext.Consumer>
        {({ language }) => (
          <FormContext.Consumer>
            {({ writeAnswer, form }) => {
              const propsToPass = {
                ...this.props,
                writeAnswer,
                activeAnswers: form[this.props.index] && form[this.props.index].value,
                language,
                values: this.props.question.CZ,
                text: this.props.question[language],
              };
              switch (this.props.question.type) {
                case 'select':
                  return (<SelectQuestion {...propsToPass} hasNextButton={false}/>);
                case 'multiselect':
                  return (<MultiselectQuestion {...propsToPass} pulseDelay={7}/>);
                case 'text':
                  return (<TextQuestion {...propsToPass}/>);
                case 'range':
                  return (<RangeQuestion {...propsToPass} pulseDelay={7}/>);
                case 'dropdown':
                  return (<DropdownQuestion {...propsToPass}/>);
                default:
                  return null;
              }
            }}
          </FormContext.Consumer>
        )}
      </LanguageContext.Consumer>
    );
  }
}

export default Question;
