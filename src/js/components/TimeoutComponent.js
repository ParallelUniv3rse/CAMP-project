import React, {Component} from 'react';
import PropTypes from 'prop-types';
import idleTimeout from 'idle-timeout';
import Button from './Button';
import LanguageContext from '../contexts/LanguageContext';

const TIME_BEFORE_RELOAD = 15;

class TimeoutComponent extends Component {
  static propTypes = {
    resetPage: PropTypes.func.isRequired,
  };

  state = {
    warningShown: false,
    countdown: TIME_BEFORE_RELOAD,
  };

  componentDidMount() {
    this.timeoutWarning = idleTimeout(this.showIdleWarning, {
      timeout: 1000 * 60 * 2, // 2 minutes
    });
  }

  showIdleWarning = () => {
    this.setState({
      warningShown: true,
      countdown: TIME_BEFORE_RELOAD,
    }, () => {
      this.resetInterval = setInterval(() => {
        if (this.state.countdown <= 0) {
          this.props.resetPage();
        } else {
          this.setState({
            countdown: this.state.countdown - 1,
          });
        }
      }, 1000);
    });
  }

  hideIdleWarning = () => {
    clearInterval(this.resetInterval);
    this.timeoutWarning.reset();
    this.setState({
      warningShown: false,
    });
  }

  render() {
    if (!this.state.warningShown) return null;
    return (
      <LanguageContext.Consumer>
        {({ copy }) => (
          <div className="modal show d-block modal--timeout" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-modal="true">
            <div className="modal-dialog modal-dialog-centered" role="document">
              <div className="modal-content">
                <div className="modal-body">
                  <div className="text-center h5">
                    <span dangerouslySetInnerHTML={{ __html: copy.timeout.text.replace('%countdown%', `<span className="countdown">${this.state.countdown}</span>`) }}></span>
                    <div>
                      <Button type="primary-outline" onClick={this.hideIdleWarning} className={'mt-5'}>
                        {copy.timeout.continue}
                      </Button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      </LanguageContext.Consumer>
    );
  }
}

export default TimeoutComponent;
