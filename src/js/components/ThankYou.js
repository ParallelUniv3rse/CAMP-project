/* eslint-disable react/prop-types,arrow-parens,operator-linebreak */
import React from 'react';
import Button from './Button';
import AppContext from '../contexts/AppContext';
import LanguageContext from '../contexts/LanguageContext';

class ThankYou extends React.Component {
  static contextType = AppContext;

  render() {
    return (
      <LanguageContext.Consumer>
        {({ copy }) => (
          <div className="swiper-slide question">
            <div className="question__content">
              <h1 className="question__title">{copy.end.thankyou}</h1>
              <h2>{copy.end.text.replace('%count%', window.count)}</h2>
              <div className="question__actions">
                <Button type="secondary" onClick={() => window.location.reload()}>
                  {copy.finish}
                </Button>
              </div>
            </div>
          </div>
        )}
      </LanguageContext.Consumer>
    );

  }
}

export default ThankYou;
