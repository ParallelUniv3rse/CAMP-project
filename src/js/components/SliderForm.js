import React from 'react';
import Swiper from 'swiper/dist/js/swiper.js';
import axios from 'axios';
import moment from 'moment';
import {TweenMax} from 'gsap';
import LanguageContext from '../contexts/LanguageContext';
import ScrollToPlugin from 'gsap/ScrollToPlugin';
import Question from "./Question";
import {default as data} from '../../../app/models/data';
import ThankYou from "./ThankYou";
import AppContext from '../contexts/AppContext';
import {padNumber} from '../helpers/formatting';
import LanguageSelector from "./LanguageSelector";

class SliderForm extends React.Component {
  static contextType = AppContext;

  state = {
    activeSlide: -1,
  };

  componentDidMount() {
    const _this = this;
    this.swiper = new Swiper(this.swiperRef, {
      allowTouchMove: false,
      autoHeight: true,
      effect: 'fade',
      pagination: {
        el: '.swiper-pagination',
        type: 'progressbar',
      },
      navigation: {
        nextEl: '.question-next',
        prevEl: '.question-prev',
      },
      on: {
        slideChange() {
          const swiper = this;
          _this.setActiveSlide(swiper.realIndex);
        },
        slideChangeTransitionStart() {
          const swiper = this;
          TweenMax.to(window, swiper.params.speed / 1000, { scrollTo: 0 });
        },
        reachEnd() {
          const swiper = this;
          if (swiper && swiper.slides.length > 1) {
            Promise.all([
              _this.props.submitForm({
                // insert metadata
                variant: _this.context.variant,
                duration: moment().diff(_this.props.startTimestamp, 'seconds'),
              }),
              new Promise(resolve => setTimeout(resolve, 10000)), // min 10s timeout
            ])
              .then(_this.context.resetPage);
          }
        },
      },
    });
  }

  setActiveSlide = (num) => new Promise((resolve, reject) => {
    this.setState({
      activeSlide: num,
    }, resolve);
  });

  handleBackClick = (e) => {
    if (e) e.preventDefault();
    this.swiper.slidePrev();
  }

  handleNextClick = (e) => {
    if (e) e.preventDefault();
    this.swiper.slideNext();
  }

  render() {
    return (
      <LanguageContext.Consumer>
        {({ updateLanguage, language, copy }) => (
          <div className="section__content" id="slider">
            <div className="swiper-container swiper-container-fade" ref={(ref) => this.swiperRef = ref}>
              <div className="swiper-wrapper">
                {data.questions.map((question, index) => (
                  <Question
                    key={index}
                    index={index}
                    question={question}
                    isActive={this.state.activeSlide === index}
                    handleNextClick={this.handleNextClick}
                  />
                ))}
                <ThankYou/>
              </div>
              {/* Add Pagination */}
              <div className="swiper-pagination"></div>
              {/* Add Arrows */}
              {this.context.env === 'development' &&
              <React.Fragment>
                <div className="question-next swiper-button-next"></div>
                <div className="question-prev swiper-button-prev"></div>
              </React.Fragment>
              }
            </div>
            {this.swiper && (this.swiper.realIndex + 1 !== this.swiper.slides.length) &&
            <React.Fragment>
              <div className="slider-info">
                <a href="#" className="mr-4 back" onClick={this.handleBackClick}><i className="icon ion-ios-arrow-round-back"></i> {copy.back}</a>
                <aside className="counter square">
                  <span className="counter__current">{padNumber(this.state.activeSlide + 1)}</span>/<span className="counter__total">{padNumber(this.swiper.slides.length - 1 || 0)}</span>
                </aside>
              </div>
              <div className="slider-info" style={{ right: "auto", left: "20px" }}>
                <LanguageSelector updateLanguage={updateLanguage} language={language}/>
                <a href="#" className="ml-4 exit" onClick={this.context.resetPage}><i className="icon ion-ios-close"></i> {copy.leave}</a>
              </div>
            </React.Fragment>
            }
          </div>
        )}
      </LanguageContext.Consumer>

    );
  }
}

export default SliderForm;
