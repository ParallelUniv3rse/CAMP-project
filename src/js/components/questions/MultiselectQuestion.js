/* eslint-disable arrow-parens */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import _xor from 'lodash.xor';
import QuestionFactory from './QuestionFactory';
import Answer from '../Answer';

class MultiselectQuestion extends Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    question: PropTypes.object.isRequired,
    values: PropTypes.shape({
      title: PropTypes.string.isRequired,
      answers: PropTypes.array.isRequired,
    }).isRequired,
    text: PropTypes.shape({
      title: PropTypes.string.isRequired,
      answers: PropTypes.array.isRequired,
    }).isRequired,
    handleSkip: PropTypes.func,
    handleAnswer: PropTypes.func,
    handleNext: PropTypes.func,
    getActiveAnswers: PropTypes.func,
    isAnswered: PropTypes.func,
    isActive: PropTypes.func,
    renderButtons: PropTypes.func,
  };

  getActiveAnswers = () => {
    let current = this.props.getActiveAnswers();

    if (!current) { // if both are null or undefined for multiselect
      current = [];
    }
    return current;
  }

  isAnswered = () => {
    const current = this.getActiveAnswers();
    return current.length !== 0;
  }

  isActive = (answer, weakFind = false) => {
    const current = this.getActiveAnswers();
    if (weakFind) { // if answer is somewhere in text of answers
      return current.findIndex((string) => string.includes(answer)) > -1;
    } else { // if answer matches text of answers 100%
      return current.indexOf(answer) > -1;
    }
  }

  handleOtherAnswer = (answer) => (parallellPromise = null) => (e) => {
    console.log(answer);
    this.props.handleAnswer(_xor(this.getActiveAnswers().filter((item) => !(item.includes('Jiné') && item !== answer)), [answer]))(parallellPromise)(e);
  }

  render() {
    return (
      <React.Fragment>
        <h1 className="question__title">{this.props.text.title}</h1>
        <div className="question__answers">
          {this.props.values.answers.map((value, index) => (
            <Answer key={index} type="card" multiselect={true} text={this.props.text.answers[index]} value={value} isActive={this.isActive(value)} onClick={this.props.handleAnswer(_xor(this.getActiveAnswers(), [value]))}/>
          ))}
          {this.props.question.hasOtherAsAnswer &&
          <Answer type="other" multiselect={true} text={this.props.language === 'EN' ? 'Other' : 'Jiné'} value={'Jiné'} isActive={this.isActive('Jiné', true)} onChange={this.handleOtherAnswer}/>
          }
        </div>
        {this.props.renderButtons(this.isAnswered)}
      </React.Fragment>
    );
  }
}

export default QuestionFactory(MultiselectQuestion);
