/* eslint-disable arrow-parens */
import React, {Component} from 'react';
import PropTypes from 'prop-types';
import QuestionFactory from './QuestionFactory';
import Answer from '../Answer';

class SelectQuestion extends Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    question: PropTypes.object.isRequired,
    values: PropTypes.shape({
      title: PropTypes.string.isRequired,
      answers: PropTypes.array.isRequired,
    }).isRequired,
    text: PropTypes.shape({
      title: PropTypes.string.isRequired,
      answers: PropTypes.array.isRequired,
    }).isRequired,
    handleSkip: PropTypes.func,
    handleAnswer: PropTypes.func,
    handleNext: PropTypes.func,
    getActiveAnswers: PropTypes.func,
    isAnswered: PropTypes.func,
    isActive: PropTypes.func,
    renderButtons: PropTypes.func,
  };

  // "Overriding" the function from factory
  handleAnswer = (answer) => (parallellPromise = null) => (e) => {
    this.props.handleAnswer(answer)(parallellPromise)(e).then(() => this.props.handleNext());
  }

  render() {
    return (
      <React.Fragment>
        <h1 className="question__title">{this.props.text.title}</h1>
        <div className="question__answers">
          {this.props.values.answers.map((value, index) => (
            <Answer key={index} type="card" text={this.props.text.answers[index]} value={value} isActive={this.props.isActive(value)} onClick={this.handleAnswer(value)}/>
          ))}
        </div>
        {this.props.renderButtons()}
      </React.Fragment>
    );
  }
}

export default QuestionFactory(SelectQuestion);
