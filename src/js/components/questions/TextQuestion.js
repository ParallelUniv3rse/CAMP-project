import React, {Component} from 'react';
import PropTypes from 'prop-types';
import QuestionFactory from './QuestionFactory';
import Answer from '../Answer';

class TextQuestion extends Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    question: PropTypes.object.isRequired,
    values: PropTypes.shape({
      title: PropTypes.string.isRequired,
    }).isRequired,
    text: PropTypes.shape({
      title: PropTypes.string.isRequired,
    }).isRequired,
    handleSkip: PropTypes.func,
    handleAnswer: PropTypes.func,
    handleNext: PropTypes.func,
    getActiveAnswers: PropTypes.func,
    isAnswered: PropTypes.func,
    isActive: PropTypes.func,
    renderButtons: PropTypes.func,
  };

  render() {
    return (
      <React.Fragment>
        <h1 className="question__title">{this.props.text.title}</h1>
        <div className="question__answers" style={{ width: '100%' }}>
          <Answer type="text" onChange={this.props.handleAnswer} label={this.props.text.title}/>
        </div>
        {this.props.renderButtons()}
      </React.Fragment>
    );
  }
}

export default QuestionFactory(TextQuestion);
