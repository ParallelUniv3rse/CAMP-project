/* eslint-disable react/prop-types, arrow-parens */
import React from 'react';
import PropTypes from 'prop-types';
import LanguageContext from '../../contexts/LanguageContext';
import {pulse} from '../../helpers/animation';
import Button from '../Button';

// time to give the user before flashing skip button
const TIME_TO_THINK = 5;

export default function QuestionFactory(WrappedQuestion) {
  class QuestionPrototype extends React.Component {
    static propTypes = {
      index: PropTypes.number.isRequired,
      question: PropTypes.object.isRequired,
      values: PropTypes.object.isRequired,
      text: PropTypes.object.isRequired,
      handleNextClick: PropTypes.func.isRequired,
      writeAnswer: PropTypes.func.isRequired,
      pulseSkip: PropTypes.bool,
      pulseDelay: PropTypes.number,
      activeAnswers: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.array,
      ]),
    };

    static defaultProps = {
      hasNextButton: true,
      pulseSkip: true,
    };

    state = {
      answerBuffer: null,
    };

    handleSkip = (e) => {
      if (e && !e.defaultPrevented) e.preventDefault();
      this.setState({
        answerBuffer: null,
      }, () => {
        this.handleNext();
      });
    }

    handleAnswer = (value) => (parallellPromise = null) => (e) => {
      if (e && !e.defaultPrevented) e.preventDefault();

      return Promise.all([new Promise((resolve, reject) => {
        this.setState({
          answerBuffer: value,
        }, resolve);
      }), parallellPromise]);
    }

    handleNext = (e) => {
      if (e && !e.defaultPrevented) e.preventDefault();
      this.props.writeAnswer(this.props.index, this.props.values.title, this.state.answerBuffer, () => {
        this.props.handleNextClick();
      });
    }

    getActiveAnswers = () => {
      let current = this.props.activeAnswers;
      if (this.state.answerBuffer) { // if we have more recent answers in buffer
        current = this.state.answerBuffer;
      }

      return current;
    }

    getReadingTime = () => {
      let words = 0;
      words += this.props.text.title.split(' ').length;
      words += this.props.text.answers ? this.props.text.answers.reduce((acc, val) => acc + val.split(' ').length, 0) : 0;
      // average reading speed is 200 words per minute
      // return neading time in seconds
      return words / 200 * 60;
    }

    isAnswered = () => {
      const current = this.getActiveAnswers();
      return !!current;
    }

    isActive = (answer) => {
      const current = this.getActiveAnswers();
      return current === answer;
    }

    componentDidUpdate(prevProps) {
      // just switched to active
      if ((this.props.isActive === true) && (prevProps.isActive === false)) {
        if (this.props.pulseSkip && this.skipButtonRef) {
          const skipButton = this.skipButtonRef.root;
          this.pulseTimeout = setTimeout(() => {
            pulse(skipButton, '#f8f9fa');
          }, (this.getReadingTime() + (this.props.pulseDelay || TIME_TO_THINK)) * 1000);
        }
      } else if ((this.props.isActive === false) && (prevProps.isActive === true)) { // just switched to inactive
        if (this.pulseTimeout) {
          clearTimeout(this.pulseTimeout);
        }
      }
    }

    // you can pass overriden functions from the child as function props
    renderButtons = (isAnswered = this.isAnswered, handleSkip = this.handleSkip, handleNext = this.handleNext) => {
      return (
        <LanguageContext.Consumer>
          {({ copy }) => (
            <div className="question__actions">
              {!this.props.question.required &&
              <Button type="secondary" ref={ref => this.skipButtonRef = ref} onClick={handleSkip}>
                {copy.skip}
              </Button>
              }
              {this.props.hasNextButton &&
              <Button type="primary" icon="ion-ios-arrow-round-forward" disabled={!isAnswered()} onClick={handleNext}>
                {copy.next}
              </Button>
              }
            </div>
          )}
        </LanguageContext.Consumer>
      );
    }

    render() {
      return (
        <div className={`swiper-slide question question--${this.props.question.type}`}>
          <div className="question__content">
            <WrappedQuestion
              {...this.props}
              handleSkip={this.handleSkip}
              handleAnswer={this.handleAnswer}
              handleNext={this.handleNext}
              getActiveAnswers={this.getActiveAnswers}
              isAnswered={this.isAnswered}
              isActive={this.isActive}
              renderButtons={this.renderButtons}
            />
          </div>
        </div>
      );
    }
  }

  return QuestionPrototype;
}
