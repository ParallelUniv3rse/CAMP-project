import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Swiper from 'swiper/dist/js/swiper';
import QuestionFactory from './QuestionFactory';
import Answer from '../Answer';

class DropdownQuestion extends Component {
  static propTypes = {
    question: PropTypes.object.isRequired,
    values: PropTypes.shape({
      title: PropTypes.string.isRequired,
      answers: PropTypes.array.isRequired,
    }).isRequired,
    text: PropTypes.shape({
      title: PropTypes.string.isRequired,
      answers: PropTypes.array.isRequired,
    }).isRequired,
    index: PropTypes.number.isRequired,
    handleSkip: PropTypes.func,
    handleAnswer: PropTypes.func,
    handleNext: PropTypes.func,
    getActiveAnswers: PropTypes.func,
    isAnswered: PropTypes.func,
    isActive: PropTypes.func,
    renderButtons: PropTypes.func,
  };

  componentDidMount() {
    this.swiper = new Swiper(this.swiperRef, {
      slidesPerView: 'auto',
      centeredSlides: true,
      slideActiveClass: 'active',
      grabCursor: true,
      slideToClickedSlide: true,
      nested: true,
      touchStartForcePreventDefault: true,
      on: {
        slideChange: this.handleSlideChange,
      },
      navigation: {
        nextEl: '.answer-next',
        prevEl: '.answer-prev',
      },
    });
    this.handleSlideChange();
    this.swiperRef.addEventListener('touchstart', (e) => {
      e.preventDefault();
    });
  }

  handleSlideChange = () => {
    const slideEl = this.swiper.slides[this.swiper.realIndex];
    this.props.handleAnswer(slideEl.dataset.value)()();
  }

  render() {
    return (
      <React.Fragment>
        <h1 className="question__title">{this.props.text.title}</h1>
        <div className='question__answers swiper'>
          <div className="swiper-container" ref={ref => this.swiperRef = ref}>
            <div className="swiper-wrapper align-items-center">
              {this.props.values.answers.map((value, index) => (
                <Answer key={index} type="card" text={this.props.text.answers[index]} value={value} isActive={this.props.isActive(value)} className='swiper-slide answer--card--small'/>
              ))}
            </div>
          </div>
          {/* Add Arrows */}
          <div className="answer-next swiper-button-white swiper-button-next"></div>
          <div className="answer-prev swiper-button-white swiper-button-prev"></div>
        </div>
        {this.props.renderButtons()}
      </React.Fragment>
    );
  }
}

export default QuestionFactory(DropdownQuestion);
