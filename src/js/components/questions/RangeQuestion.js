import React, {Component} from 'react';
import PropTypes from 'prop-types';
import QuestionFactory from './QuestionFactory';
import Answer from '../Answer';

class RangeQuestion extends Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    question: PropTypes.shape({
      default: PropTypes.number,
      max: PropTypes.number,
    }).isRequired,
    values: PropTypes.shape({
      title: PropTypes.string.isRequired,
      minLabel: PropTypes.string.isRequired,
      maxLabel: PropTypes.string.isRequired,
    }).isRequired,
    text: PropTypes.shape({
      title: PropTypes.string.isRequired,
      minLabel: PropTypes.string.isRequired,
      maxLabel: PropTypes.string.isRequired,
    }).isRequired,
    handleSkip: PropTypes.func,
    handleAnswer: PropTypes.func,
    handleNext: PropTypes.func,
    getActiveAnswers: PropTypes.func,
    isAnswered: PropTypes.func,
    isActive: PropTypes.func,
    renderButtons: PropTypes.func,
  };

  render() {
    return (
      <React.Fragment>
        <h1 className="question__title">{this.props.text.title}</h1>
        <div className="question__answers" style={{ width: '100%' }}>
          <Answer type="range" onChange={this.props.handleAnswer} question={this.props.question} values={this.props.values} text={this.props.text}/>
        </div>
        {this.props.renderButtons()}
      </React.Fragment>
    );
  }
}

export default QuestionFactory(RangeQuestion);
