import React from 'react';
import PropTypes from 'prop-types';

const buttonClasses = {
  primary: 'btn--custom btn--custom--primary',
  'primary-outline': 'btn--custom btn--custom--outline-primary',
  secondary: 'btn--custom btn--custom--outline-light',
};

class Button extends React.Component {
  static propTypes = {
    type: PropTypes.oneOf(['primary', 'primary-outline', 'secondary']),
    icon: PropTypes.string,
  };

  render() {
    const ButtonTag = this.props.href ? 'a' : 'button';
    return (
      <ButtonTag {...this.props} ref={ref => this.root = ref} className={`btn btn--large-round btn--hover-grow ${buttonClasses[this.props.type] || ''} ${this.props.className || ''}`}>
        {this.props.children}
        {this.props.icon &&
        <React.Fragment>
          &nbsp;<i className={`icon ${this.props.icon}`}></i>
        </React.Fragment>
        }
      </ButtonTag>
    );
  }
}

export default Button;
