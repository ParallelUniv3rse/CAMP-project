/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import CardAnswer from './answers/CardAnswer';
import TextAnswer from './answers/TextAnswer';
import RangeAnswer from './answers/RangeAnswer';
import OtherAnswer from './answers/OtherAnswer';

class Answer extends React.Component {
  static propTypes = {
    type: PropTypes.string.isRequired,
  };

  render() {
    switch (this.props.type) {
      case 'card':
        return (<CardAnswer {...this.props} />);
      case 'text':
        return (<TextAnswer {...this.props}/>);
      case 'range':
        return (<RangeAnswer {...this.props}/>);
      case 'other':
        return (<OtherAnswer {...this.props}/>);
      default:
        return null;
    }
  }
}

export default Answer;
