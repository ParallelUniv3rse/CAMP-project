/* eslint-disable react/prop-types */
import React from 'react';

class TextAnswer extends React.Component {
  constructor(props) {
    super(props);
    this.state = { value: '' };
  }

  handleChange = (e) => {
    const { value } = e.target;
    this.setState({ value }, () => {
      this.props.onChange(value)()();
    });
  }

  render() {
    return (
      <div className={`answer--text ${this.props.className || ''}`}>
        <label htmlFor={`input${this.props.label}`} className="sr-only">${this.props.label}</label>
        <input className="form-control form-control-lg" type="text" id={`input${this.props.label}`} value={this.state.value} onChange={this.handleChange} placeholder="Odpověď..."/>
      </div>
    );
  }
}

export default TextAnswer;
