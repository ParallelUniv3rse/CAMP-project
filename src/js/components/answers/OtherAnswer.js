/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import CardAnswer from './CardAnswer';

class OtherAnswer extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    multiselect: PropTypes.bool,
  };

  state = { value: '' };

  handleChange = (e) => {
    const { value } = e.target;
    this.setState({ value }, () => {
      this.props.onChange(`${this.props.value}: ${value}`)()();
    });
  }

  handleClick = (...props) => {
    if (this.props.isActive) {
      this.setState({
        value: '',
      });
    } else {
      this.input.focus();
    }
    return this.props.onChange(`${this.props.value}: ${this.state.value}`)(...props);
  }

  render() {
    return (
      <CardAnswer {...this.props} onClick={this.handleClick} type="card">
        <input ref={ref => this.input = ref} onClick={e => this.props.isActive && e.stopPropagation()} className="form-control text-center" type="text" value={this.state.value} onChange={this.handleChange}/>
      </CardAnswer>
    );
  }
}

export default OtherAnswer;
