/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import {buttonClickAnimation} from '../../helpers/animation';

class CardAnswer extends React.Component {
  static propTypes = {
    text: PropTypes.string.isRequired,
    value: PropTypes.string.isRequired,
    multiselect: PropTypes.bool,
  };

  componentDidMount() {
    this.clickAnimation = buttonClickAnimation(this.card);
  }

  handleClick = (e) => {
    e.preventDefault();
    const animatingIn = !this.props.isActive;

    if (this.props.onClick) {
      this.props.onClick(new Promise((resolve, reject) => {
        const onComplete = () => resolve();
        this.clickAnimation.vars.onComplete = onComplete;
        this.clickAnimation.vars.onReverseComplete = onComplete;

        if (animatingIn) {
          this.clickAnimation.play();
        } else {
          this.clickAnimation.reverse();
        }
      }))();
    }
  }

  render() {
    return (
      <div
        className={`card answer--card ${this.props.isActive ? 'active' : ''} ${this.props.className || ''}`}
        ref={(ref) => this.card = ref}
        onClick={this.handleClick}
        data-value={this.props.value}
      >
        <div className="card-body flex-column">
          <p className="card-text h4">{this.props.text}</p>
          {this.props.children}
        </div>
        {this.props.multiselect &&
        <div className='answer__badge'>
          <i className='icon ion-ios-checkmark'></i>
        </div>
        }
      </div>
    );
  }
}

export default CardAnswer;
