/* eslint-disable react/prop-types */
import React from 'react';
import Slider from 'rc-slider';

class RangeAnswer extends React.Component {
  constructor(props) {
    super(props);
    this.defaultValue = props.question.default ? props.question.default : 1;
  }

  handleChange = (value) => {
    this.props.onChange(value)()();
  }

  componentDidMount() {
    this.handleChange(this.defaultValue);
  }

  render() {
    const max = this.props.question.max ? this.props.question.max : 5;
    return (
      <div className={`answer--range ${this.props.className || ''}`}>
        <Slider
          dots
          min={1}
          max={max}
          marks={{
            1: this.props.text.minLabel || '',
            [max]: this.props.text.maxLabel || '',
          }}
          defaultValue={this.defaultValue}
          onAfterChange={this.handleChange}/>
      </div>
    );
  }
}

export default RangeAnswer;
