import React from 'react';
import axios from 'axios';
import PropTypes from 'prop-types';

const FormContext = React.createContext();

export class FormProvider extends React.Component {
  static propTypes = {
    variant: PropTypes.string.isRequired,
    children: PropTypes.any,
  };

  state = {
    form: {},
  };

  submitForm = (metadata = {}) => {
    const formData = new FormData();
    Object.values(this.state.form).map((storedValue, index) => formData.append(index, storedValue.value === null ? '' : storedValue.value));
    formData.append(Object.values(this.state.form).length, JSON.stringify(metadata));
    return axios({
      method: 'post',
      url: '/submit',
      data: formData,
    });
  }

  writeAnswer = (key, question, value, callback = () => {
  }) => {
    this.setState({
      form: {
        ...this.state.form,
        [key]: {
          question,
          value,
        },
      },
    }, () => {
      console.log(this.state.form);
      callback();
    });
  }

  render() {
    return (
      <FormContext.Provider value={{
        form: this.state.form,
        submitForm: this.submitForm,
        writeAnswer: this.writeAnswer,
      }}>
        {this.props.children}
      </FormContext.Provider>
    );
  }
}

export default FormContext;
