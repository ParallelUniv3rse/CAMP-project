# CAMP feedback questionnaire
## Application requirements
- NodeJS 10.13.0 or higher
- globally installed gulp-cli (``npm i -g gulp-cli``)
## Building the application
The app build is done automatically on deployment when running ``npm run deploy``. If you with to build it separately, follow instructions below:

- make sure your environment meets the Application requirements
- make sure you have all application dependencies installed ``npm i``
- run the build script in production mode ``NODE_ENV=production gulp build``
## Deploying the application: 
- ensure you have pm2 process manager installed globally. ``npm i -g pm2``
- first time deployment only: run ``npm i`` manually, otherwise the deployment script fails because of non-installed dependency.
- run ``npm run deploy``
- the server will be now running on default HTTP port 8888. The port can be changed by specifying ``PORT=<port>`` when running the above comand.
