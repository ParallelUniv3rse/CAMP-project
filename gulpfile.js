const path = require('path'),
  del = require('del'),
  gulp = require('gulp'),
  nodemon = require('gulp-nodemon'),
  plumber = require('gulp-plumber'),
  sass = require('gulp-sass'),
  browserSync = require('browser-sync').create(),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  webpack = require('webpack-stream'),
  webpackSrc = require('webpack'),
  rename = require('gulp-rename'),
  download = require('gulp-download');

const serverConfig = require('./config/config');

const options = {
  app: {
    nodemon: {
      script: 'app.js',
      ext: 'js json',
      ignore: ['src/**/*.*', 'dist/**/*.*', 'app/views/**/*.js', 'sassPaths.js', 'gulpfile.js', 'node_modules/', 'webpack.config.js'],
      env: {
        NODE_ENV: 'development',
        DEBUG: 'appname:*',
      },
    },
    browserSync: {
      proxy: `http://localhost:${serverConfig.port}`,
      port: serverConfig.port + 1000,
      notify: true,
      tunnel: false,
    },
  },
  apis: {
    locales: 'https://localise.biz/api/export/locale/{locale}.json?key=GZsHEvCjB8t4648weNQy45X_pUeaKrtQ&pretty',
  },
  html: {
    src: 'app/views',
    ext: '.ejs',
  },
  copyFiles: [
    path.join(serverConfig.paths.src, '/static/**/*.*'),
    path.join(serverConfig.paths.src, '/browserconfig.xml'),
    path.join(serverConfig.paths.src, '/manifest.json'),
    path.join(serverConfig.paths.src, '/robots.txt'),
    path.join(serverConfig.paths.src, '/sitemap.xml'),
  ],
  js: {
    entryFile: 'client.js',
  },
  styles: {
    // minify: true,
    // concat: false,
    autoprefixerCompatibility: ['last 3 versions', '> 1%'],
    sassOptions: {
      outputStyle: 'compressed',
      includePaths: require('./sassPaths'),
      /*
       ------- nested:(indented like scss)-------

       .widget-social {
       text-align: right; }
       .widget-social a,
       .widget-social a:visited {
       padding: 0 3px;
       color: #222222;
       color: rgba(34, 34, 34, 0.77); }

       ------- expanded:(classic css) -------

       .widget-social {
       text-align: right;
       }
       .widget-social a,
       .widget-social a:visited {
       padding: 0 3px;
       color: #222222;
       color: rgba(34, 34, 34, 0.77);
       }

       ------- compact -------

       .widget-social { text-align: right; }
       .widget-social a, .widget-social a:visited { padding: 0 3px; color: #222222; color: rgba(34, 34, 34, 0.77); }

       ------- compressed:(minified) -------

       .widget-social{text-align:right}.widget-social a,.widget-social a:visited{padding:0 3px;color:#222222;color:rgba(34,34,34,0.77)}
       */
    },
  },
};

function styles() {
  return gulp.src([path.join(serverConfig.paths.src, 'css', '/**/*.scss')], {base: path.join(serverConfig.paths.src, 'css')})
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass(options.styles.sassOptions).on('error', sass.logError))
    .pipe(autoprefixer({ browsers: options.styles.autoprefixerCompatibility }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(path.join(serverConfig.paths.dist, 'css')))
    .pipe(browserSync.stream({ match: '**/*.css' }));
}

function scripts() {
  return gulp.src([path.join(serverConfig.paths.src, 'js', options.js.entryFile)])
    .pipe(plumber())
    .pipe(webpack(require('./webpack.config.js'), webpackSrc))
    .on('error', function handleError() {
      this.emit('end'); // Recover from errors
    })
    .pipe(gulp.dest(path.join(serverConfig.paths.dist, 'js')));
}

function downloadLocales(done) {
  for (const [key, value] of Object.entries(serverConfig.languages)) {
    download(options.apis.locales.replace('{locale}', key))
      .pipe(rename(`${key}.json`))
      .pipe(gulp.dest('app/models/locales/'));
  }
  done();
}

/**
 * Copies specific files defined in gulp options.
 */
function copyFiles() {
  return gulp.src(options.copyFiles, { base: serverConfig.paths.src, allowEmpty: true })
    .pipe(gulp.dest(serverConfig.paths.dist));
}

function reload(done) {
  browserSync.reload();
  done();
}

/**
 * cleans the dist directory
 */
function clean(done) {
  del.sync(serverConfig.paths.dist);
  done();
}

function watch(done) {
  gulp.watch(path.join(serverConfig.paths.src, 'css', '/**/*.scss')).on('all', styles);
  gulp.watch(path.join(options.html.src, `/**/*${options.html.ext}`)).on('all', gulp.series(reload));
  gulp.watch(path.join(serverConfig.paths.src, 'js', '**/*.js')).on('all', gulp.series(scripts, reload));
  gulp.watch(options.copyFiles).on('all', gulp.series(copyFiles, reload));
  done();
}

function startBrowserSync(done) {
  setTimeout(() => {
    browserSync.init(options.app.browserSync);
    done();
  }, 1000);
}

gulp.task('nodemon', (done) => {
  let called = false;
  const server = nodemon(options.app.nodemon);

  server.on('start', () => {
    if (!called) {
      called = true;
      done();
    }
  });

  server.on('restart', () => {
    console.log('restarted');
    // TODO: is this delay needed? - yes, to wait for nodemon after restart – promise maybe?
    setTimeout(() => {
      gulp.series(reload);
    }, 1000);
  });

  server.on('crash', () => {
    console.error('Application has crashed!\n');
    server.emit('restart', 5); // restart the server in 5 seconds
  });
});

gulp.task('locales', gulp.series(downloadLocales));

gulp.task('build', gulp.series(clean, gulp.parallel(copyFiles, styles, scripts)));

gulp.task('server', gulp.series('nodemon', startBrowserSync, (done) => {
  done();
}));
gulp.task('deploy', gulp.series('build'));

gulp.task('default', gulp.series('build', 'server', watch));
