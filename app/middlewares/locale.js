const config = require('../../config/config');

module.exports = function () {
  return function locale(req, res, next) {
    const lc = req.locale.toString();
    const locale = {
      langCode: {
        full: lc,
        short: lc.split('_')[0],
      },
      ...config.languages[lc],
    };
    const urlPath = req.path;
    console.log('--------------');
    console.log(urlPath);
    console.log(req.originalUrl);
    console.log('--------------');

    // ---- check for lang setting in the url ----

    /**
     * Setting template variables and persisting cookies
     */
    req.locale = locale; // pass to other middleware
    res.locals.locale = locale; // make the current locale available in templates
    res.locals.availableLocales = config.languages; // make locale list available in templates
    try {
      res.locals.copy = require(`../../app/models/locales/${req.locale.langCode.full}.json`);
    } catch (err) {
      res.locals.copy = require('../../app/models/locales/en_US.json'); // fallback copy for languages that are missing a language file (eg. cs_CZ locale)
    }
    return next();
  };
};
