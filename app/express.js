const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const glob = require('glob');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const compress = require('compression');
const methodOverride = require('method-override');
const createLocaleMiddleware = require('express-locale');
const formidable = require('formidable');
const moment = require('moment');
const striptags = require('striptags');
const locale = require('./middlewares/locale');
const config = require('../config/config');
const data = require('./models/data');
const spreadsheets = require('./models/modules/spreadsheets');
const { getRouteForLocale } = require('./namedRoutes');

const abTest = {
  SITE: 0,
};

function getCurrentTestVariant() {
  const minimum = Object.keys(abTest).map(key => ({
    key,
    value: abTest[key],
  })).sort((a, b) => a.value - b.value)[0];
  abTest[minimum.key] += 1;
  console.log('time:', new Date().toISOString());
  console.log('variants served: ', abTest);
  return minimum.key;
}


function getCountryFlag(cc) {
  // Mild sanity check.
  if (cc.length !== 2) {
    return cc;
  }

  // Convert char to Regional Indicator Symbol Letter
  function risl(chr) {
    return String.fromCodePoint(0x1F1E6 - 65 + chr.toUpperCase().charCodeAt(0));
  }

  // Create RISL sequence from country code.
  return risl(cc[0]) + risl(cc[1]);
}

function stripTrailingSlash(str) {
  return str.replace(/\/$/, '');
}

module.exports = (app) => {
  const env = process.env.NODE_ENV || 'development';

  app.set('views', `${config.root}/app/views`);
  app.set('view engine', 'ejs');
  app.use(expressLayouts);
  app.set('layout', 'layouts/layout');

  app.locals.ENV = env;
  app.locals.ENV_DEVELOPMENT = env === 'development';
  app.locals.config = config;
  app.locals.data = data;

  // app.use(favicon(config.root + '/public/img/favicon.ico'));
  app.use(logger('dev'));
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: true,
  }));
  app.use(cookieParser());
  app.use(compress());
  app.use(express.static(`${config.root}/${config.paths.dist}`));
  app.use(methodOverride());

  app.locals.striptags = striptags;
  // ------- add url builder for named routes -------
  app.locals.buildUrl = (localeShort, routeName, append = null) => {
    const url = stripTrailingSlash(`/${localeShort}${getRouteForLocale(routeName, localeShort)}`);
    return `${url}${append || ''}`;
  };
  app.locals.getCountryFlag = getCountryFlag;


  // ------- locales --------
  const getLocaleMap = () => { // makes plugin use "en_US" for "en" only locale, "cs_CZ" for "cs" etc.
    const result = {};
    for (const key of Object.keys(config.languages)) {
      result[key.split('_')[0]] = key;
    }
    return result;
  };

  /**
   * locale resolution
   */
  app.use('/', createLocaleMiddleware({
    priority: ['default'],
    lookups: {
      langQuery: (req) => {
        const { lang } = req.query;
        if (lang !== undefined && lang.length === 2) {
          // console.log('langLookup', lang);
          return getLocaleMap()[lang];
        }
        // console.log('langLookup', false);
        return undefined;
      },
    },
    map: getLocaleMap(),
    default: 'cs_CZ',
    allowed: Object.keys(config.languages),
  }));
  /**
   * locale persistance
   */
  app.use(locale());

  // ------- end locales ----------

  app.get('/', (req, res, next) => {
    res.render('index', {
      pageName: 'landing',
      title: 'CAMP | zpětná vazba',
      variant: getCurrentTestVariant(),
      count: Object.values(abTest).reduce((accumulator, currentValue) => accumulator + currentValue),
    });
  });

  app.post('/submit', (req, res, next) => {
    const form = new formidable.IncomingForm();
    const promise = new Promise((resolve, reject) => {
      form.parse(req, (err, fields, files) => {
        resolve(fields);
      });
    }).then((fields) => {
      if (env !== 'development') {
        spreadsheets.writeData([moment().format('MM/DD/YYYY HH:mm:ss'), ...Object.values(fields)]);
      } else {
        console.log(fields);
      }
      res.json({ success: true });
    });
  });

  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  app.use((err, req, res, next) => {
    res.status(err.status || 500);
    res.render('error', {
      pageName: 'error',
      message: err.message,
      error: err,
      title: 'error',
    });
  });

  return app;
};
