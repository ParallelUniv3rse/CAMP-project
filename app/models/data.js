module.exports = {
  questions: [
    {
      type: 'select',
      CZ: {
        title: 'Kolik je vám let?',
        answers: [
          '<18',
          '18-25',
          '26-35',
          '36-45',
          '46-60',
          '61+',
        ],
      },
      EN: {
        title: 'How old are you?',
        answers: [
          '<18',
          '18-25',
          '26-35',
          '36-45',
          '46-60',
          '61+',
        ],
      },
    },
    {
      type: 'select',
      CZ: {
        title: 'Jsem',
        answers: [
          'Muž',
          'Žena',
        ],
      },
      EN: {
        title: 'I am',
        answers: [
          'Male',
          'Female',
        ],
      },
    },
    {
      type: 'dropdown',
      CZ: {
        title: 'V jaké části Prahy bydlíte?',
        answers: [
          'Nebydlím v Praze',
          'Praha-východ',
          'Praha-západ',
          'Praha 1',
          'Praha 2',
          'Praha 3',
          'Praha 4',
          'Praha 5',
          'Praha 6',
          'Praha 7',
          'Praha 8',
          'Praha 9',
          'Praha 10',
          'Praha 11',
          'Praha 12',
          'Praha 13',
          'Praha 14',
          'Praha 15',
          'Praha 16',
          'Praha 17',
          'Praha 18',
          'Praha 19',
          'Praha 20',
          'Praha 21',
          'Praha 22',
        ],
      },
      EN: {
        title: 'In which part of Prague do you live?',
        answers: [
          'I don\'t live in Prague',
          'Prague-east',
          'Prague-west',
          'Prague 1',
          'Prague 2',
          'Prague 3',
          'Prague 4',
          'Prague 5',
          'Prague 6',
          'Prague 7',
          'Prague 8',
          'Prague 9',
          'Prague 10',
          'Prague 11',
          'Prague 12',
          'Prague 13',
          'Prague 14',
          'Prague 15',
          'Prague 16',
          'Prague 17',
          'Prague 18',
          'Prague 19',
          'Prague 20',
          'Prague 21',
          'Prague 22',
        ],
      },
    },
    {
      type: 'multiselect',
      CZ: {
        title: 'Jsem',
        answers: [
          'Student VŠ',
          'Architekt, urbanista, plánovač...',
          'Politik či úředník',
          'Široká veřejnost',
        ],
      },
      EN: {
        title: 'I am',
        answers: [
          'College student',
          'Architect, urbanist, city planner...',
          'Politican or clerk',
          'General public',
        ],
      },
    },
    {
      type: 'select',
      CZ: {
        title: 'Jak často CAMP navštěvujete?',
        answers: [
          'Jsem tu poprvé',
          'Několikrát do týdne',
          'Několikrát do měsíce',
          'Několikrát do roka',
        ],
      },
      EN: {
        title: 'How often do you visit CAMP?',
        answers: [
          'This is my first time',
          'Several times a week',
          'Several times a month',
          'Several times a year',
        ],
      },
    },
    {
      type: 'multiselect',
      hasOtherAsAnswer: true,
      CZ: {
        title: 'Za jakým účelem sem chodíte?',
        answers: [
          'Chodím na výstavy',
          'Chodím na přednášky, diskuze, filmové projekce',
          'Chodím do kavárny',
          'Chodím do CAMPu na schůzky nebo pracovat',
        ],
      },
      EN: {
        title: 'Why do you come here?',
        answers: [
          'I come for the exhibitions',
          'I come for the talks, discussions, movie screenings',
          'I come for the coffee shop',
          'I come for meetings or to work here',
        ],
      },
    },
    {
      type: 'select',
      CZ: {
        title: 'Jak často využíváte naši kavárnu?',
        answers: [
          'Skoro pokaždé když tu jsem',
          'Tu a tam',
          'Kavárnu nevyužívám',
        ],
      },
      EN: {
        title: 'How often do you use our coffee shop?',
        answers: [
          'Almost every time',
          'Every now and then',
          'I don\'t use it',
        ],
      },
    },
    {
      type: 'text',
      CZ: {
        title: 'Je něco, co byste v kavárně rádi viděli?',
      },
      EN: {
        title: 'It there anything you would like to see in the coffee shop?',
      },
    },
    {
      type: 'select',
      CZ: {
        title: 'Jak často se účastníte přednášek a debat v CAMPu?',
        answers: [
          'Často když nějaké jsou (když se o nich dozvím)',
          'Tu a tam když je něco zajímavého',
          'Na akcích jsem nikdy nebyl',
        ],
      },
      EN: {
        title: 'How often do you attend events at CAMP?',
        answers: [
          'Often if there are some (and I know about them)',
          'Now and then when there\'s something interesting',
          'I\'ve never been to any events',
        ],
      },
    },
    {
      type: 'multiselect',
      hasOtherAsAnswer: true,
      CZ: {
        title: 'Odkud se dovídáte o našich akcích?',
        answers: [
          'Instagram',
          'Facebook',
          'Články v médiích',
          'Od známých',
          'V rámci mé práce',
        ],
      },
      EN: {
        title: 'Where do you get to know about our events from?',
        answers: [
          'Instagram',
          'Facebook',
          'Articles in the media',
          'From friends',
          'From my work',
        ],
      },
    },
    {
      type: 'select',
      CZ: {
        title: 'Jak vám přijdou výstavy srozumitelné?',
        answers: [
          'Žádnou z výstav jsem neviděl',
          'Výstavy jsou příliš zjednodušující',
          'Výstavy jsou dobře vybalancované - ani moc odborné, ani moc zjednodušující',
          'Výstavy jsou příliš odborné',
        ],
      },
      EN: {
        title: 'How comprehensible do you find the exhibitions?',
        answers: [
          'I\'ve never seen any',
          'They are oversimplified',
          'They are well ballanced - not too professional, not oversimplified',
          'They are too professional',
        ],
      },
    },
    {
      type: 'text',
      CZ: {
        title: 'Je nějaké téma, které by nemělo v příštím roce chybět?',
      },
      EN: {
        title: 'Is there any topic that should not be left out next year?',
      },
    },
    {
      type: 'range',
      default: 3,
      CZ: {
        title: 'Jak se vám zamlouvá výběr naší knihovny?',
        minLabel: 'Nesedí mi',
        maxLabel: 'Skvělý',
      },
      EN: {
        title: 'How do you like the contents of our library?',
        minLabel: 'Not interesting',
        maxLabel: 'I love it',
      },
    },
    {
      type: 'text',
      CZ: {
        title: 'Je něco co byste rádi v naší knihovně viděli?',
      },
      EN: {
        title: 'Is there anything you miss in our library?',
      },
    },
    {
      type: 'text',
      CZ: {
        title: 'Máte nějaké další nápady nebo poznámky které byste s námi chtěli sdílet?',
      },
      EN: {
        title: 'Do you have any other ideas or comments you would like to share?',
      },
    },
  ],
};
