const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets']; // you can add more scopes according to your permission need. But in case you chang the scope, make sure you deleted the ~/.credentials/sheets.googleapis.com-nodejs-quickstart.json file
const TOKEN_PATH = 'token.json';

class Authentication {
  authenticate() {
    return this.getClientSecret()
      .then(this.authorize);
  }

  getClientSecret() {
    return new Promise((resolve, reject) => {
      // Load client secrets from a local file.
      fs.readFile('credentials.json', (err, content) => {
        if (err) {
          console.log('Error loading client secret file:', err);
          reject(err);
        }
        // Authorize a client with credentials, then call the Google Sheets API.
        resolve(JSON.parse(content));
      });
    });
  }

  authorize(credentials) {
    const { client_secret: clientSecret, client_id: clientId, redirect_uris: redirectUris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(clientId, clientSecret, redirectUris[0]);

    return new Promise((resolve, reject) => {
      // Check if we have previously stored a token.
      fs.readFile(TOKEN_PATH, (err, token) => {
        if (err) {
          this.getNewToken(oAuth2Client).then((oauth2ClientNew) => {
            resolve(oauth2ClientNew);
          }, (err) => {
            reject(err);
          });
        } else {
          oAuth2Client.setCredentials(JSON.parse(token));
          resolve(oAuth2Client);
        }
      });
    });
  }

  getNewToken(oauth2Client) {
    return new Promise((resolve, reject) => {
      const authUrl = oauth2Client.generateAuthUrl({
        access_type: 'offline',
        scope: SCOPES,
      });
      console.log('Authorize this app by visiting this url: \n ', authUrl);
      const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout,
      });
      rl.question('\n\nEnter the code from that page here: ', (code) => {
        rl.close();
        oauth2Client.getToken(code, (err, token) => {
          if (err) {
            console.log('Error while trying to retrieve access token', err);
            reject();
          }
          oauth2Client.setCredentials(token);
          this.storeToken(token);
          resolve(oauth2Client);
        });
      });
    });
  }

  storeToken(token) {
    fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
      if (err) console.error(err);
      console.log('Token stored to', TOKEN_PATH);
    });
  }
}

module.exports = new Authentication();
