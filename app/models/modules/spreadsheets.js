const { google } = require('googleapis');
const authentication = require('./authentication');

class Spreadsheets {
  writeData(data = []) {
    return authentication.authenticate().then((auth) => {
      const sheets = google.sheets('v4');
      sheets.spreadsheets.values.append({
        auth,
        spreadsheetId: '1Lf-dFPnuh_JoTrsE8ZuUIrVf2bms5ox0wclPjy7DCdE',
        range: 'Form responses from Site', // Change Sheet1 if your worksheet's name is something else
        valueInputOption: 'RAW',
        resource: {
          values: [data],
        },
      }, (err, response) => {
        if (err) {
          console.log(`The API returned an error: ${err}`);
        } else {
          console.log('Appended');
        }
      });
    });
  }
}

module.exports = new Spreadsheets();
