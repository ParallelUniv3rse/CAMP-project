const namedRoutes = {
  index: {
    en: '/',
    cs: '/',
  },
};

module.exports.default = namedRoutes;

module.exports.getLocaleRoutes = (shortLocale) => {
  const result = {};
  for (const [name, routesObj] of Object.entries(namedRoutes)) {
    result[name] = routesObj[shortLocale];
  }
  return result;
};

module.exports.getRouteForLocale = (routeName, shortLocale) => {
  return namedRoutes[routeName][shortLocale] || namedRoutes[routeName].en;
};
