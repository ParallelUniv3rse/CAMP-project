const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const webpack = require('webpack');

const env = process.env.NODE_ENV || 'development';

const rules = [
  {
    test: /\.m?js$/,
    exclude: /(node_modules|bower_components)/,
    use: {
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env', '@babel/preset-react'],
        plugins: [
          require('@babel/plugin-proposal-object-rest-spread'),
          require('@babel/plugin-proposal-class-properties'),
        ],
      },
    },
  },
];

module.exports = {
  mode: env,
  output: {
    filename: 'client.js',
  },
  module: {
    rules,
  },
  resolve: {
    alias: {
      jquery: 'jquery/dist/jquery.slim.min.js',
    },
  },
  optimization: {
    sideEffects: false,
    minimizer: [new UglifyJsPlugin({
      sourceMap: true,
      uglifyOptions: {
        compress: {
          warnings: false,
        },
      },
    })],
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
    }),
    // new BundleAnalyzerPlugin(),
  ],
};
