module.exports = {
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
    es6: true,
    jquery: true,
  },
  // https://github.com/standard/standard/blob/master/docs/RULES-en.md
  extends: ['airbnb-base', 'plugin:react/recommended'],
  // add your custom rules here
  'rules': {
    // allow async-await
    'generator-star-spacing': 0,
    "indent": [2, 2, {"SwitchCase": 1, "VariableDeclarator": 1}],
    "one-var": "off",
    "no-plusplus": "off",
    "no-return-assign": "off",
    "max-len": "off",
    "operator-linebreak": "off",
    "no-underscore-dangle": "off",
    'comma-dangle': ['error', 'always-multiline'],
    "global-require":"off",
    "import/no-extraneous-dependencies": "off",
    "class-methods-use-this": "off",
    "object-curly-spacing": 0,
    "no-else-return": "off",
    "no-unused-vars": "warn",
    "no-shadow": "off",
    "no-restricted-syntax": [
      "error",
      "ForInStatement",
      "LabeledStatement",
      "WithStatement"
    ],
    "no-param-reassign": "off",
    "import/no-dynamic-require": "off",
  },
};
