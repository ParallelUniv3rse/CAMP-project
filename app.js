const express = require('express'),
  config = require('./config/config');

const app = express();

module.exports = require('./app/express')(app);

app.listen(config.port, () => {
  console.log(`Express server listening on port ${config.port}`);
});
