const path = require('path'),
  rootPath = path.normalize(`${__dirname}/..`),
  env = process.env.NODE_ENV || 'development';

const globals = {
  root: rootPath,
  app: {
    name: 'blocks-website',
  },
  paths: {
    src: 'src',
    dist: 'dist',
  },
  languages: {
    en_US: {
      short: 'EN',
      name: 'English',
    },
    cs_CZ: {
      short: 'CZ',
      name: 'Česky',
    },
  },
};

const config = {
  development: Object.assign({}, globals, {
    port: process.env.PORT || 3000,
  }),

  test: Object.assign({}, globals, {
    port: process.env.PORT || 3000,
  }),

  production: Object.assign({}, globals, {
    port: process.env.PORT || 80,
  }),
};

module.exports = config[env];
